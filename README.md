# GM5 Avaliação Front-End

- Prazo para entrega: dois dias úteis
- Enviar o link do repositório para
rh@gm5.com.br;
amattos@gm5.com.br;
hribeiro@gm5.com.br;

## Enunciado

- Implementar a página enviada (psd), de forma responsiva, lendo o arquivo JSON dentro da pasta assets.

## Comportamento no tablet

- A Seção "Nossos serviços" terá o primeiro item como destaque, ocupando as 12 colunas e os 2 itens restantes dividirão o espaço da segunda linha, ocupando cada um 6 colunas.


## Requisitos não funcionais:
- Utilizar HTML, CSS, javascript e a grid do bootstrap, versão 3 ou 4.
- Desenvolver a página sem utilizar templates prontos.
- Submeter o projeto completo, autocontido, funcionando sem a necessidade de acesso à internet
- Incluir as instruções necessárias para configuração/utilização do projeto, no formato de readme


## Linguagens e frameworks aceitos
- JQuery
- Angular 2+
- ReactJS
- Less/Sass

###### GM5 Desenvolvimento
