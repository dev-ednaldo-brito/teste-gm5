var gulp = require('gulp');
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var cssmin = require('gulp-cssmin');
var imagemin = require('gulp-imagemin');
var spritesmith = require('gulp.spritesmith');
var browserSync = require('browser-sync').create();

gulp.task('sass', function () {
  return gulp.src(['dev/scss/**/*.scss'])
        .pipe(sass().on('error', sass.logError))
        //.pipe(concat('main.css'))
        .pipe(cssmin())
        //.pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('assets/css/'))
});

gulp.task('scripts', function () {
  return gulp.src(['dev/js/*.js'])
     .pipe(uglify())
    .pipe(concat('main.min.js'))
    .pipe(gulp.dest('assets/js/'))
});

gulp.task('images', function () {
  gulp.src('dev/img/**/*')
    .pipe(gulp.dest('assets/img/'))
});

gulp.task('sprites', function () {
  var spriteData = gulp.src('dev/img/sprite/**/*')
    .pipe(spritesmith({
      imgName: 'sprite-site.png',
      cssName: '_sprite.scss',
      padding: 10
    }))

  spriteData.img.pipe(gulp.dest('assets/img/'));
  spriteData.css.pipe(gulp.dest('dev/scss/partials/base/'));
});

gulp.task('watch', function() {
  gulp.watch('dev/img/**', ['images']);
  gulp.watch('dev/js/**/*.js',['scripts']);
  gulp.watch('dev/scss/**/**/*.scss',['sass']);
  gulp.watch('dev/img/sprite/**', ['sprites']);
  gulp.watch('dev/scss/**/*.scss', ['sass']).on('change', browserSync.reload);
  gulp.watch("/*.html").on('change', browserSync.reload);
});

gulp.task('browser-sync', function () {
  var files = [
    '*.html',
    'assets/css/**/*.css',
    'assets/js/**/*.js',
    'dev/scss/**/*.scss'
  ];

  browserSync.init(files, {
    server: {
      baseDir: './'
    }
  });
});


gulp.task('default', ['images', 'watch', 'sass', 'scripts', 'sprites', 'browser-sync']);
gulp.task('faster', ['images', 'watch', 'sass', 'scripts', 'sprites' ]);
gulp.task('js', ['scripts']);
